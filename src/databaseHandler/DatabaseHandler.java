/**
 * 
 */
package databaseHandler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sachingadagi.Word;

/**
 * @author tuxer 
 * 			
 * 			Database handler class Acts as contoller between the backend
 *         	and UI All the functions pertaining to the database are written here
 *
 */
public class DatabaseHandler {

	ResultSet resultSet = null;
	Statement statement = null;
	Connection con = null;

	static DatabaseHandler dbh = null; // singleton

	private DatabaseHandler() {}

	public static DatabaseHandler getInstance() {

		if (dbh == null) {
			return new DatabaseHandler();
		} else {
			return dbh;
		}
	}

	public boolean connect() {
		try {
			Class.forName("org.sqlite.JDBC");
			String dbURL = "jdbc:sqlite:D:\\all-words.db3";
			con = DriverManager.getConnection(dbURL);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != resultSet && null != statement) {
					resultSet.close();
					statement.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		return false;

	}

	public List<String> getWords() throws SQLException {
		
		List<String> wordList = new ArrayList<>();

		if(con == null)
			this.connect();

		statement = con.createStatement();
		resultSet = statement.executeQuery("SELECT * FROM all_words LIMIT 15 ");

		while (resultSet.next()) {
			wordList.add(resultSet.getString("word"));
		}

		return wordList;

	}

	public Word getWord(String find_word) throws SQLException {
		Word word = new Word();
		statement = con.createStatement();
		resultSet = statement.executeQuery("SELECT * FROM all_words WHERE word = '" + find_word + "' ");

		word.setId(resultSet.getInt("_id"));
		word.setIs_in_barrons(resultSet.getShort("is_in_barrons_list"));
		word.setIs_in_kalpans_list(resultSet.getShort("is_in_kaplans_list"));
		word.setIs_in_manhattans_list(resultSet.getShort("is_in_manhattans_list"));
		word.setWord(resultSet.getString("word"));
		word.setSentence(resultSet.getString("sentence"));
		word.setMeaning(resultSet.getString("meaning"));
		return word;
	}

}
