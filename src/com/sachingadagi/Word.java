/**
 * 
 */
package com.sachingadagi;

/**
 * @author tuxer
 *  The model class that best describes the "word"
 *
 */

public class Word {
	
	int id;
	
	String 	word,
			meaning,
			sentence;
	
	short 	is_in_barrons_list,
			is_in_kalpans_list,
			is_in_manhattans_list;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the word
	 */
	public String getWord() {
		return word;
	}

	/**
	 * @param word the word to set
	 */
	public void setWord(String word) {
		this.word = word;
	}

	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}

	/**
	 * @param meaning the meaning to set
	 */
	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}

	/**
	 * @return the sentence
	 */
	public String getSentence() {
		return sentence;
	}

	/**
	 * @param sentence the sentence to set
	 */
	public void setSentence(String sentence) {
		this.sentence = sentence;
	}

	/**
	 * @return the is_in_barrons
	 */
	public short getIs_in_barrons() {
		return is_in_barrons_list;
	}

	/**
	 * @param is_in_barrons the is_in_barrons to set
	 */
	public void setIs_in_barrons(short is_in_barrons_list) {
		this.is_in_barrons_list = is_in_barrons_list;
	}

	/**
	 * @return the is_in_kalpans_list
	 */
	public short getIs_in_kalpans_list() {
		return is_in_kalpans_list;
	}

	/**
	 * @param is_in_kalpans_list the is_in_kalpans_list to set
	 */
	public void setIs_in_kalpans_list(short is_in_kalpans_list) {
		this.is_in_kalpans_list = is_in_kalpans_list;
	}

	/**
	 * @return the is_in_manhattans_list
	 */
	public short getIs_in_manhattans_list() {
		return is_in_manhattans_list;
	}

	/**
	 * @param is_in_manhattans_list the is_in_manhattans_list to set
	 */
	public void setIs_in_manhattans_list(short is_in_manhattans_list) {
		this.is_in_manhattans_list = is_in_manhattans_list;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		String s = this.word ;
		if(this.is_in_kalpans_list == 1)
			s = s + " [KALPAN]";
		if(this.is_in_barrons_list == 1)
			s = s + "[BARRON]";
		if(this.is_in_manhattans_list == 1)
			s = s + "[MANHATTAN]";
			return s ;
	}
	
	
}
