package com.sachingadagi;

import java.awt.Checkbox;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import com.sun.javafx.collections.transformation.FilteredList;
import com.sun.javafx.collections.transformation.Matcher;
import com.sun.org.apache.xerces.internal.impl.dv.DatatypeValidator;

import databaseHandler.DatabaseHandler;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * Main.java Starting point of the application.
 */

public class Main extends Application implements Initializable {

	@FXML
	ListView<String> lv_words;

	@FXML
	TextField searchbar;

	@FXML
	Label lbl_word;

	@FXML
	Label lbl_meaning;

	@FXML
	Label lbl_sentence;

	@FXML
	CheckBox cb_all;

	@FXML
	CheckBox cb_barrons;

	@FXML
	CheckBox cb_kalpans;

	@FXML
	CheckBox cb_manhattan;

	List<String> values = null;
	List<Word> wordList = null;
	ObservableList<String> oList = null;
	DatabaseHandler dbh = null;

	private void initGUI() {
		
		cb_all.setSelected(true);
		cb_barrons.setSelected(true);
		cb_kalpans.setSelected(true);
		cb_manhattan.setSelected(true);
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setTitle("Welcome");

			// Load the UI from Home.fxml
			BorderPane a = (BorderPane) FXMLLoader.load(getClass().getResource("Home.fxml"));
			Scene s = new Scene(a);

			primaryStage.setScene(s);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		initGUI();

		// connect to the database using singleton pattern
		dbh = DatabaseHandler.getInstance();
		dbh.connect();

		try {
			values = dbh.getWords();
			oList = FXCollections.observableArrayList(values);

		} catch (Exception e) {
			e.printStackTrace();
		}

		lv_words.setItems(FXCollections.observableList(values));

		// for custom listview
		lv_words.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
			@Override
			public ListCell<String> call(ListView<String> list) {
				return new AttachmentListCell();
			}
		});

		// listener to the search input
		searchbar.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				search((String) oldValue, (String) newValue);
			}

			//(local) search method that manipulates the listview with observable list 'subentries'
			private void search(String oldVal, String newVal) {

				if (oldVal != null && (newVal.length() < oldVal.length())) {
					lv_words.setItems(oList);
				}
				String value = newVal;
				ObservableList<String> subentries = FXCollections.observableArrayList();
				for (Object entry : lv_words.getItems()) {
					boolean match = true;
					String entryText = (String) entry;
					if (!entryText.startsWith(value)) {
						match = false;
						// break;
					}
					if (match) {
						subentries.add(entryText);
					}
				}
				lv_words.setItems(subentries);
			}
		});

		// Setting up listener to the listitem

		lv_words.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				
				Word w = null;
				try {
					w = dbh.getWord(newValue);
					if (w != null) {
						lbl_word.setText(w.getWord());
						lbl_sentence.setText(w.getSentence());
						lbl_meaning.setText(w.getMeaning());
					}
					System.out.println(w);
				} catch (SQLException e) {
					e.printStackTrace();

				}

			}
		});

		/*
		 * ***************************************** 
		 * 		LISTENERS FOR CHECKBOXES
		 * *****************************************
		 */
		cb_all.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					if (newValue == true) {
					cb_barrons.setSelected(true);
					cb_kalpans.setSelected(true);
					cb_manhattan.setSelected(true);
				}
			}
		});

		cb_manhattan.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

				if (newValue == false) {
					cb_all.setSelected(false);
				}

				if (cb_kalpans.isSelected() && cb_manhattan.isSelected() && cb_barrons.isSelected()) {
					cb_all.setSelected(true);
				}

				refreshList();
			}

		});

		cb_kalpans.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				
				if (newValue == false) {
					cb_all.setSelected(false);
				}

				if (cb_kalpans.isSelected() && cb_manhattan.isSelected() && cb_barrons.isSelected()) {
					cb_all.setSelected(true);
				}
				refreshList();
			}
		});

		cb_barrons.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				
				if (newValue == false) {
					cb_all.setSelected(false);
				}

				if (cb_kalpans.isSelected() && cb_manhattan.isSelected() && cb_barrons.isSelected()) {
					cb_all.setSelected(true);
				}
				refreshList();
			}
		});
	}

	private void refreshList() {
		// TODO Complete this function

	}

	// IGNORE AttachmentListCell class
	// This class lets us create custom listview (like the adapter in android)
	private static class AttachmentListCell extends ListCell<String> {
		Label n = new Label();

		@Override
		public void updateItem(String item, boolean empty) {
			super.updateItem(item, empty);
			if (empty) {

				setText(null);

			} else {
				setText(item);

			}
		}
	}
}
